package by.itstep.hw22


import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_month.view.*
import java.lang.RuntimeException

// TODO: Rename parameter arguments, choose names that match
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FragmentMonth : Fragment() {

    interface OnFragmentDataListener {
        fun action()
    }

    lateinit var listener: OnFragmentDataListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentDataListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentDataListener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_month, container, false)
        view.january.setOnClickListener {
            Log.d("FragmentMonth","Orientation check")
            listener.action()
        }
        return view
    }
}


