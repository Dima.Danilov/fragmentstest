package by.itstep.hw22

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

class FragmentTest : AppCompatActivity(), FragmentMonth.OnFragmentDataListener {
    val fragmentMonth = FragmentMonth()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_test)
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainer2, fragmentMonth).commit()
    }

    override fun action() {
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d("FragmentTest", "Portrait orientation")
            callAnotherActivity()
        } else {
            Log.d("FragmentTest", "Landscape orientation")
            changeFragment()
        }
    }

    private fun changeFragment() {
        Log.d("FragmentTest", "Replace fragments")
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        Log.d("FragmentTest", "Trying to replace")
          ft.replace(R.id.fragmentContainer2, FragmentDays()).commit()

    }

    private fun callAnotherActivity() {
        Log.d("FragmentTest", "Call another activity")
        startActivity(Intent(this, Days::class.java))
    }
}
